<?php
// src/AppBundle/Form/PhotoType.php
 namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;


 
class PhotoType extends AbstractType
{
   public function buildForm(FormBuilderInterface $builder, array $options)
   {
       $builder->add('nom')
                ->add('image', FileType::class, array('label' => 'fichier .png ou .jpg à importer'))
                ->add("Exporter l'image", SubmitType::class)
                ;
   }
}
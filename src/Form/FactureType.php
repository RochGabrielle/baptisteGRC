<?php
// src/Form/FactureType.php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use App\Entity\Facture;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\DateType;


class FactureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $dateId = date("Ymd");
        $dateDuJour = date('Y-m-d');
        $date = \DateTime::createFromFormat('Y-m-d', $dateDuJour);
        $builder
            ->add('date_de_facture', DateType::class,array(
                'widget' => 'single_text',
                ))
            ->add('numero_facture',null,array(
                'required'  => false,
                'data' => $dateId,               
                ))
            ->add('montant')
            ->add('CreerLaFacture', SubmitType::class)
            ;
        
    }
}
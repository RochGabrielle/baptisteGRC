<?php
// src/AppBundle/Form/RegistrationType.php
 
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
 
class InscriptionType extends AbstractType
{
   public function buildForm(FormBuilderInterface $builder, array $options)
   {
       $builder->add('nom')
                ->add('rue')
                ->add('code_postal')
                ->add('ville')
                ->add('Ajouter', SubmitType::class)
                ;
   }
 
   public function getParent()
   {
    return 'FOS\UserBundle\Form\Type\RegistrationFormType';
       
 
   }
 
   public function getBlockPrefix()
   {
       return 'app_user_registration';
   }
}
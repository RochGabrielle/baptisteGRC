<?php
// src/Form/AdresseType.php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use App\Entity\Adresse;
use Symfony\Component\OptionsResolver\OptionsResolver;


class AdresseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('rue')
            ->add('code_postal')
            ->add('ville')
        ;
    }
}
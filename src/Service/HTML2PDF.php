<?php
/**
 * Service that creates a pdf file
 * 
 * @category Service
 * @author roch Gabrielle <roch_gabrielle@yahoo.fr>
 */
namespace App\Service;

/**
 * Class Html2Pdf
 * 
 * @category Class
 * @author   roch Gabrielle <roch_gabrielle@yahoo.fr>
 */
class HTML2PDF
{
    private $_pdf;

    /**
     * Instantiates the html2pdf object
     *
     * @param string $orientation orientation
     * @param string $format      format
     * @param string $lang        language
     * @param string $unicode     unicode
     * @param string $encoding    encoding
     * @param string $margin      margin
     *
     * @return null
     */
    public function create($orientation = null, $format = null, $lang = null, $unicode = null, $encoding = null, $margin = null)
    {
        $this->_pdf = new \Html2Pdf(
            $orientation ? $orientation : $this->orientation,
            $format ? $format : $this->format,
            $lang ? $lang : $this->lang,
            $unicode ? $unicode : $this->unicode,
            $encoding ? $encoding : $this->encoding,
            $margin ? $margin : $this->margin
        );
    }

    /**
     * Generate a pdf
     *
     * @param string $template path to a template
     * @param string $name     name of the file
     *
     * @return create a file $name.pdf
     */
    public function generatePdf($template, $name)
    {
        $this->_pdf->writeHTML($template);
        return $this->pdf->Output($name . '.pdf');

    }
}

<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FactureRepository")
 */
class Facture
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $date_de_facture;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $numero_facture;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Commande", mappedBy="facture")
     */
    private $commande;

    /**
     * @ORM\Column(type="float")
     */
    private $montant;

    public function __construct()
    {
        $this->commande = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDateDeFacture(): ?\DateTimeInterface
    {
        return $this->date_de_facture;
    }

    public function setDateDeFacture(\DateTimeInterface $date_de_facture): self
    {
        $this->date_de_facture = $date_de_facture;

        return $this;
    }

    public function getNumeroFacture(): ?string
    {
        return $this->numero_facture;
    }

    public function setNumeroFacture(string $numero_facture): self
    {
        $this->numero_facture = $numero_facture;

        return $this;
    }

    /**
     * @return Collection|Commande[]
     */
    public function getCommande(): Collection
    {
        return $this->commande;
    }

    public function addCommande(Commande $commande): self
    {
        if (!$this->commande->contains($commande)) {
            $this->commande[] = $commande;
            $commande->setFacture($this);
        }

        return $this;
    }

    public function removeCommande(Commande $commande): self
    {
        if ($this->commande->contains($commande)) {
            $this->commande->removeElement($commande);
            // set the owning side to null (unless already changed)
            if ($commande->getFacture() === $this) {
                $commande->setFacture(null);
            }
        }

        return $this;
    }

    public function getMontant(): ?float
    {
        return $this->montant;
    }

    public function setMontant(float $montant): self
    {
        $this->montant = $montant;

        return $this;
    }
}

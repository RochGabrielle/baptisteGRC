<?php
/**
 * Welcome page controller
 * 
 * @category Controller
 * @author   Roch Gabrielle <roch_gabrielle@yahoo.fr>
 */
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Welcome class
 * 
 * @category Class
 * @author   Roch Gabrielle <roch_gabrielle@yahoo.fr>
 */
class WelcomeController extends Controller
{
    /**
     * Displays the homepage
     * 
     * @return page
     */
    public function welcomePage()
    {
        return $this->render('welcome/welcome.html.twig');
    }
}

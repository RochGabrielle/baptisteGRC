<?php
/**
 * Administration controller
 *
 * @category Controller
 * @author roch Gabrielle <roch_gabrielle@yahoo.fr>
 */
namespace App\Controller;

use App\Entity\Commande;
use App\Entity\Facture;
use App\Entity\User;
use App\Entity\Photo;
use App\Form\FactureType;
use App\Form\InscriptionType;
use App\Form\PhotoType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Class Administration
 *
 * @category Class
 * @author   roch Gabrielle <roch_gabrielle@yahoo.fr>
 */
class AdministrationController extends Controller
{
    /**
     * Welcome page of administration, displays all the orders
     *
     * @param object $request get object of the form
     *
     * @return page
     */
    public function administrationPage(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Commande::class);
        $command = $repository->findAll();

        return $this->render(
            'administration/administration.html.twig',
            array(
                'commandes' => $command,
            )
        );
    }

    /**
     * Add a customer form
     *
     * @param object $request the
     *
     * @return page
     */
    public function ajoutClient(Request $request)
    {
        $repository = $this->getdoctrine()->getRepository(User::class);
        $clients = $repository->findAll(); 
        $user = new User();
        $form = $this->createForm(InscriptionType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$commande` variable has also been updated
            $user = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('app_admin_page');
        }

        return $this->render(
            'administration/ajoutClient.html.twig',
            array(
                'form' => $form->createView(),
                'clients' => $clients,
            )
        );
    }

    /**
     * Displays a customer file
     *
     * @param object $clientId the
     *
     * @return page
     */
    public function ficheClient($clientId)
    {
        $repository = $this->getDoctrine()->getRepository(User::class);
        $client = $repository->findOneById($clientId);

        return $this->render(
            'administration/ficheClient.html.twig',
            array(
                'client' => $client,
            )
        );
    }

    /**
     * View of the administration page of an invoice
     *
     * @param int $commandeId id of an invoice
     *
     * @return page
     */
    public function administrationVueCommande($commandeId, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Commande::class);
        $command = $repository->findOneById($commandeId);

        $photo = new Photo();

        $form = $this->createForm(PhotoType::class, $photo);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$commande` variable has also been updated
            $photo = $form->getData();
            $photo->setCommande($command);
            $file = $photo->getImage();
            $extension = $file->guessClientExtension();
            $date = date("dmY");
            $nom = $photo->getNom().$date;
            $fileName = $nom.'.'.$extension;
            $photo->setAdresseUrl("/photo/".$fileName);

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($photo);
            $file->move("photo/", $fileName);
            $entityManager->flush();

            return $this->redirectToRoute('app_admin_page');
        }



        return $this->render(
            'administration/administrationCommmande.html.twig',
            array(
                'commande' => $command,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * Editing of a bill
     *
     * @param int    $commandeId id of the command
     * @param object $request    the get object
     *
     * @return to administration route
     */
    public function editFacture($commandeId, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Commande::class);
        $commande = $repository->findOneById($commandeId);
        $dateDuJour = date("d.m.y");

        $facture = new Facture();
        $formFacture = $this->createForm(FactureType::class, $facture);

        $formFacture->handleRequest($request);

        if ($formFacture->isSubmitted() && $formFacture->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$commande` variable has also been updated
            $facture = $formFacture->getData();
            $commande->setFacture($facture);

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($facture);
            $entityManager->persist($commande);
            $entityManager->flush();

            return $this->redirectToRoute(
                'app_admin_commande-facture',
                array(
                    'commandeId' => $commandeId,
                )
            );
        }

        return $this->render(
            'administration/editFacture.html.twig', array(
                'form' => $formFacture->createView(),
                'commande' => $commande,
                'dateDuJour' => $dateDuJour,
            )
        );
    }

    /**
     * Lance la facture de commande
     *
     * @param int $commandeId id of the commmand
     *
     * @return page
     */
    public function administrationFactureCommande($commandeId)
    {
        $path = '/wamp/www/baptiste/bptgrc/public/facture/';
        $repository = $this->getDoctrine()->getRepository(Commande::class);
        $command = $repository->findOneById($commandeId);
        $c = array(10, 15, 10, 15);
        $html2pdf = new \Spipu\Html2Pdf\Html2Pdf('P', 'A4', 'fr', true, 'UTF-8', $c);
        $pdf = $this->render(
            'administration/facture.html.twig',
            array(
                'commande' => $command,
            )
        );
        $html2pdf->writeHTML($pdf);
        $html2pdf->output($path . $command->getId() . '.pdf', 'F');

        return $this->redirectToRoute('app_admin_page');
    }

    /**
     * Visualiser une facture de commande
     *
     * @param int $commandeId id of the commmand
     *
     * @return page
     */
    public function administrationFactureVoir($commandeId)
    {
        $response = new BinaryFileResponse('facture/'.$commandeId.'.pdf');
        $response->headers->set('Content-Type', 'application/pdf');
        return $response;
    }
}

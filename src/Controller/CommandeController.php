<?php
/**
 * Commande controller
 * 
 * @category Controller
 * @author   roch Gabrielle <roch_gabrielle@yahoo.fr>
 */
namespace App\Controller;

use App\Entity\Adresse;
use App\Entity\Commande;
use App\Form\AdresseType;
use App\Form\CommandeType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Command controller class
 * 
 * @category Class
 */
class CommandeController extends Controller
{
    /**
     * Display and persist command
     * 
     * @param object $request the get request
     * 
     * @return render page
     */
    public function commandePage(Request $request)
    {
        $commande = new Commande();
        $adresse = new Adresse();
        $form = $this->createForm(CommandeType::class, $commande);
        $formAdresse = $this->createForm(AdresseType::class, $adresse);
        //Getting user information
        $user = $this->getUser();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$commande` variable has also been updated
            $commande = $form->getData();
            $adresse = $formAdresse->getData();
            $dateCommande = date("m.d.y");
            $commande->setDateDeCommande(\DateTime::createFromFormat("m.d.y", $dateCommande));
            $commande->setStatut('1');
            $commande->setUser($user);

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($commande);
            $entityManager->flush();

            return $this->redirectToRoute('app_CommandSuccess_page');
        }

        return $this->render('commande/commande.html.twig', array(
            'form' => $form->createView(),
            'formAdresse' => $formAdresse->createView(),
        ));
    }

    /**
     * Display command success page
     */
    public function commandeSuccessPage()
    {
        return $this->render('commande/commandeSuccess.html.twig');
    }

    /**
     * Display the order passed by the logged in user
     */
    public function commandeSuivi(Request $request)
    {
        $user = $this->getUser();
        $repository = $this->getDoctrine()->getRepository(Commande::class);
        $command = $repository->findBy(
            ['user' => $user]
        );

        return $this->render(
            'commande/commandeSuivi.html.twig', array(
                'commandes' => $command,
            )
        );

    }

    /**
     * View of the administration page of an invoice
     *
     * @param int $commandeId id of an invoice
     *
     * @return page
     */
    public function commandeVue($commandeId)
    {
        
        $repository = $this->getDoctrine()->getRepository(Commande::class);
        $command = $repository->findOneById($commandeId);

        



        return $this->render(
            '/commande/commandeVue.html.twig',
            array(
                'commande' => $command,
            )
        );
    }

     /**
     * Visualiser une facture de commande
     *
     * @param int $commandeId id of the commmand
     *
     * @return page
     */
    public function commandeFactureVoir($commandeId)
    {
        $response = new BinaryFileResponse('facture/'.$commandeId.'.pdf');
        $response->headers->set('Content-Type', 'application/pdf');
        return $response;
    }
}
